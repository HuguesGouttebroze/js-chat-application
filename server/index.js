const express = require('express');
const app = express();
const PORT = 4000;
const http = require('http').Server(app);
const cors = require('cors');

/**
 * Socket.io to real-time connection
 */
const socketIO = require('socket.io')(http, {
    cors: {
        origin: "http://localhost:3000"
    }
})
socketIO.on('connect', (socket) => {
    console.log(`${socket.id} user on`);
    socket.on('disconnect', () => {
        console.log(`user off`)
    });
});
app.get('.api', (req, res) => {
    res.json({
        message: 'Hey Oh Hello',
    });
});
app.listen(PORT, () => {
    console.log(`Serveur écoute sur le port ${PORT}`);
});