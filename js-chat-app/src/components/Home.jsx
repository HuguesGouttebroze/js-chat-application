import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

export const Home = () => {
  const navigate = useNavigate();
  const [userName, setUserName] = useState('');

  const handleSubmit =(e) => {
    e.preventDefault();
    localStorage.setItem('pseudo', userName);
    navigate('/chat');
  }
  return (
      <>
        <form className='home_container' onSubmit={handleSubmit}>
            <h2 className='home_header'>
                Se connecter - Lancer discution
            </h2>
            <label htmlFor='username'>Pseudo utilisateur</label>
            <input 
                type='text'
                minLength={4}
                name="username"
                className='username_input'
                id='username'
                value={userName}
                onChage={(e) => setUserName(e.target.value)}
            />
            <button className='home_cta'>Connection</button>
        </form>
      </>
  );
};

